#! /bin/bash

cd $HOME/.dotfiles;
git config --bool branch.master.sync true
git commit -a;
$HOME/projects/setupcomputer/git-sync-script/git-sync;

cd $HOME/.ssh;
git config --bool branch.master.sync true
git commit -a;
$HOME/projects/setupcomputer/git-sync-script/git-sync;

for x in $HOME/projects/*; do
    echo $x;
    if [ -d "$x/.git" ]
    then
        cd $x;
        git config --bool branch.master.sync true
        $HOME/projects/setupcomputer/git-sync-script/git-sync;
    fi 
done