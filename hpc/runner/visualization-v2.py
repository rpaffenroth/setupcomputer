# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.2'
#       jupytext_version: 1.2.4
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# %%
# database = 'sqlite:////home/rcpaffenroth/data/mydatabase_many.db'
database = 'sqlite:////home/rcpaffenroth/data/for_paper_10-2-2019/mydatabase_many.db'
# database = 'sqlite:////home/rcpaffenroth/data/for_paper_random_init_10-3-2019/mydatabase_many.db'

# %%
import dataset
import pickle
import pandas as pa
import numpy as np
import matplotlib.pylab as py
import collections
import scipy.stats
import seaborn as sns

# %%
db = dataset.connect(database)
table = db['runs']
result = table.all()
dfOrig = pa.DataFrame(result)

# %%
# dfOrig

# %% [markdown]
# # Preprocessing 

# %% [markdown]
# Compute a subset, if I like.

# %%
df = dfOrig[ (dfOrig['thetaStartIndex']==0) & (dfOrig['thetaEndIndex']==-1)]
# df = dfOrig[ (dfOrig['thetaStartIndex']==-10000) & (dfOrig['thetaEndIndex']==-1)]
# df = dfOrig[ (dfOrig['thetaStartIndex']==0) & (dfOrig['thetaEndIndex']==10000)]


# %%
# colorsMarkers = ['ro-','go-','bo-','co-','mo-','yo-','ko-','#aaafffo-','#ee11bbo-','r+-','g+-','b+-','c+-','m+-','y+-','k+-','#aaafff+-','#ee11bb+-','rt-','gt-']
colors = ['r','g','b','c','m','y','k','r','g','b','c','m','y','k','r','g','b','c','m','y']
markers = ['o','o','o','o','o','o','o','+','+','+','+','+','+','+','v','v','v','v','v','v']
colorMap = dict(zip(df['name'].unique(),colors))
markerMap = dict(zip(df['name'].unique(),markers))

# %%
colorMap

# %% [markdown]
# Unpickle J and E, and add E normalized

# %%
JsNum = []
EsNum = []
for index, row in df.iterrows():
    print(row['name'])
    JsNum += [pickle.loads(row['Js'])]
    EsNum += [pickle.loads(row['Es'])]

df['Js'] = pa.Series(JsNum, index=df.index)    
df['Es'] = pa.Series(EsNum, index=df.index)    

# %% [markdown]
# SLOW CELL

# %%
ENorms = []
for index,row in df.iterrows():
    ENorms += [[]]
    for J in row['Js']:
        for i in range(J.shape[0]):
            norm = np.linalg.norm(J[i,:])
            if norm > 0:
                J[i,:] /= norm 
        U,E,VT = np.linalg.svd(J)
        ENorms[-1] += [E]
df['ENorms'] = pa.Series(ENorms, index=df.index)    

# %%
errorMap = pa.read_csv('errors.csv')
errorMap = errorMap.set_index('name')

# %% [markdown]
# #  All the data 

# %%
_,ax = py.subplots(ncols=2,figsize=[16,8])
for index, row in df.iterrows():
    for E in row['Es']:
        ax[0].plot((E/E[0]), colorMap[row['name']])
        ax[1].semilogy((E/E[0]), colorMap[row['name']])

# %%
_,ax = py.subplots(ncols=2,figsize=[16,8])
for index, row in df.iterrows():
    for E in row['ENorms']:
        ax[0].plot((E/E[0]), colorMap[row['name']])
        ax[1].semilogy((E/E[0]), colorMap[row['name']])


# %% [markdown]
# # Averages 

# %%
EAverage=collections.defaultdict(lambda : np.zeros(df['numCols'].iloc[0]))
for index, row in df.iterrows():
    for E in row['Es']:
        EAverage[row['name']] += E/E[0]

_,ax = py.subplots(ncols=2,figsize=[16,8])
for key in EAverage.keys():
    ax[0].plot(EAverage[key],colorMap[key]+markerMap[key])
    ax[1].semilogy(EAverage[key],colorMap[key]+markerMap[key])
    ax[1].set_ylim([1e-5,10])
ax[0].legend(EAverage.keys())
ax[1].legend(EAverage.keys())

# %%
EAverage=collections.defaultdict(lambda : np.zeros(df['numCols'].iloc[0]))
for index, row in df.iterrows():
    for E in row['Es']:
        EAverage[row['name']] += E/E[0]

_,ax = py.subplots(ncols=1,figsize=[16,8])
for key in EAverage.keys():
#    ax.plot(EAverage[key],colorMap[key]+markerMap[key])
     ax.semilogy(EAverage[key],colorMap[key]+markerMap[key])
     ax.set_ylim([1e-5,10])
ax.legend(EAverage.keys())
# ax[1].legend(EAverage.keys())
py.savefig('NN_CSD_averages.png')

# %%
EAverage=collections.defaultdict(lambda : np.zeros(df['numCols'].iloc[0]))
for index, row in df.iterrows():
    for E in row['ENorms']:
        EAverage[row['name']] += E/E[0]

_,ax = py.subplots(ncols=2,figsize=[16,8])
for key in EAverage.keys():
    ax[0].plot(EAverage[key],colorMap[key]+markerMap[key])
    ax[1].semilogy(EAverage[key],colorMap[key]+markerMap[key])
ax[0].legend(EAverage.keys())
ax[1].legend(EAverage.keys())


# %% [markdown]
# # Measures of dimension 

# %%
def dimension(E):
    for i in range(len(E)):
        if np.sum(E[:i])/np.sum(E) > 0.9:
            return i        
        
def dimErrorPlot(df, namesToUse=None, namesToRemove=[], average=False, EType='ENorms',errorType='best1', plot=True):
    if plot:
        py.figure(figsize=(10,10))
    dims = []
    errors = []
    for index, row in df.iterrows():
        useName = True
        if not namesToUse is None:
            if not row['name'] in namesToUse:
                useName = False
        if row['name'] in namesToRemove:
            useName = False
        if useName:
            localDims = []
            localErrors = []
            for E in row[EType]:
                localDims += [dimension(E)]
                error = 100.-float(errorMap.loc[row['name']][errorType])
                localErrors += [error]
            if average:
                localDims = [np.average(localDims)]
                localErrors = [np.average(localErrors)]
            if plot:
                py.scatter(localDims, localErrors, color=colorMap[row['name']], marker=markerMap[row['name']], label=row['name'])
            dims += localDims
            errors += localErrors
    slope,intercept,rvalue,pvalue,stderr = scipy.stats.linregress(dims, errors)
    if plot:
        # lcolor = 'gray'
        sns.regplot(dims, errors, scatter=False, color = 'gray')
        #sns.regplot(dims, errors, scatter=False, color = 'g', joint_kws={'line_kws':{'color':lcolor}})
        # py.title('P-value %f Slope %f'%(pvalue,slope) )
        py.title('P-value %8.5f'%(pvalue,) )
        #sb.set()
        #sb.jointplot(x="efRankTaylor01", y="trainacc", data=a,  kind='reg',stat_func=p, color = 'g', joint_kws={'line_kws':{'color':lcolor}})
        #py.suptitle('Training accuracy vs Taylor effective rank 0.90')
        # plt.subplots_adjust(top=0.94)
        py.xlabel('Effective rank (10% cutoff)')
        py.ylabel('Testing accuracy (%)')
        py.grid()
        py.legend()
    return {'slope':slope, 'pvalue':pvalue}


# %%
tableData = []
for tag,namesToRemove in zip(['all','outliers removed'],[[],['alexnet','squeezenet1_0','squeezenet1_1']]):
    for EType in ['ENorms','Es']:
        for errorType in ['best1','best5']:
            myDict = dimErrorPlot(df, plot=False, namesToRemove=namesToRemove,average=True,EType=EType,errorType=errorType)
            myDict['tag'] = tag
            myDict['EType'] = EType
            myDict['errorType'] = errorType 
            tableData += [myDict]
for tag,namesToUse in zip(['resnet(18,34,50,101,152)'],[['resnet18','resnet34','resnet50','resnet101','resnet152']]):
    for EType in ['ENorms','Es']:
        for errorType in ['best1','best5']:
            myDict = dimErrorPlot(df, plot=False, namesToUse=namesToUse,average=True,EType=EType,errorType=errorType)
            myDict['tag'] = tag
            myDict['EType'] = EType
            myDict['errorType'] = errorType 
            tableData += [myDict]


# %%
print('Networks Used & Normalized & Error & P-value \\\\')
print('\midrule')
for row in tableData:
    if row['EType'] == 'Es':
        norm='No'
    else:
        norm='Yes'
    print('%s & %s & %s & %8.5f \\\\'%(row['tag'],norm,row['errorType'],row['pvalue']))

# %%
dimErrorPlot(df,average=True,EType='ENorms',errorType='best1')
py.savefig('comparison_all.png')

# %%
dimErrorPlot(df,namesToRemove=['alexnet','squeezenet1_0','squeezenet1_1'], average=True,EType='ENorms',errorType='best1')
py.savefig('comparison_good.png')

# %%
numThetas = []
errors = []
py.figure(figsize=(10,10))
for index, row in df.iterrows():
#for index, row in df.iterrows():
    numThetas += [row['numTheta']]
    error = 100.-float(errorMap.loc[row['name']]['best1'])
    errors += [error]
    py.scatter([row['numTheta']], [error], color=colorMap[row['name']], label=row['name'])
sns.regplot(numThetas, errors, scatter=False, color = 'gray')      
slope,intercept,rvalue,pvalue,stderr=scipy.stats.linregress(numThetas, errors)
py.title('P-value %8.5f'%(pvalue) )
py.legend()
py.grid()
py.xlabel('Number of unknowns')
py.ylabel('Testing accuracy (%)')
py.savefig('num_theta_all.png')

# %%
numThetas = []
errors = []
namesToRemove=['alexnet','squeezenet1_0','squeezenet1_1']
py.figure(figsize=(10,10))
for index, row in df.iterrows():
#for index, row in df.iterrows():
    useName = True
    if row['name'] in namesToRemove:
        useName = False
    if useName:
        numThetas += [row['numTheta']]
        error = 100.-float(errorMap.loc[row['name']]['best1'])
        errors += [error]
        py.scatter([row['numTheta']], [error], color=colorMap[row['name']], label=row['name'])
sns.regplot(numThetas, errors, scatter=False, color = 'gray')      
slope,intercept,rvalue,pvalue,stderr=scipy.stats.linregress(numThetas, errors)
print(slope,intercept,rvalue,pvalue,stderr)
py.title('P-value %f'%(pvalue) )
py.legend()
py.grid()
py.xlabel('Number of unknowns')
py.ylabel('Testing accuracy (%)')
py.savefig('num_theta_good.png')

# %%



# %%
