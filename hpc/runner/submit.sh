#! /bin/bash
# Request a GPU
#SBATCH --gres=gpu
# Request memory
#SBATCH --mem=16G
# Pipe output to /dev/null
# ###SBATCH --output=/dev/null

# Process command line arguments
# The python script to run
SCRIPTNAME="$1"
shift 1
# A label for the output file to keep things organized
LABEL="$1"
shift 1

# A base directory for the data output
DATADIR=$HOME/data
# DATADIR=.

# The executables for python
PYTHON=$HOME/miniconda3/envs/deeplearning/bin/python
PAPERMILL=$HOME/miniconda3/envs/deeplearning/bin/papermill

# Create a temporary directory
NOW=$(date +"%F_%T")
DIR=$(mktemp -d ${DATADIR}/${LABEL}_${NOW}.XXXXX)
mkdir -p $DIR

# Copp the script there
cp $SCRIPTNAME $DIR
cd $DIR

# Print some logging information
# $@ is the remaining arguments
echo "${DIR} ${SCRIPTNAME} ${@}" > script.log

# Check is we are running a python script or a notebook
if [ "${SCRIPTNAME##*.}" = "ipynb" ]; then
    # We need to do a little additional work to get the output notebook name
    NAME=$(basename $SCRIPTNAME)
    NAME=${NAME%.*}
    srun ${PAPERMILL} ${NAME}.ipynb ${NAME}_out.ipynb "${@}" >> script.log
else
    srun ${PYTHON} $(basename $SCRIPTNAME) "${@}" >> script.log
fi

