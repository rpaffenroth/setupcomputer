import subprocess

# Rows take no additional calculations, just additional memory since all \theta derivatives are done anyway.
numRows = 4000
numCols = 2000
numJ = 10
database = 'sqlite:////home/rcpaffenroth/data/mydatabase_many.db'

# networks = ['mobilenet_v2']

networks = ['alexnet',
            'vgg11', 'vgg13', 'vgg16', 'vgg19', 
            'resnet18', 'resnet34', 'resnet50', 'resnet101', 'resnet152',  
            'squeezenet1_0', 'squeezenet1_1', 
            'densenet121', 'densenet169', 'densenet161', 'densenet201', 
            'shufflenet_v2_x1_0',
            'mobilenet_v2',
            'resnext50_32x4d', 'resnext101_32x8d']

#networks = ['resnet18','alexnet','vgg16','squeezenet1_0','densenet161','inception_v3',
#'googlenet','shufflenet_v2_x1_0','mobilenet_v2']

# networks = ['resnet18','alexnet','vgg16','squeezenet1_0','densenet161','inception_v3',
# 'googlenet','shufflenet_v2_x1_0','mobilenet_v2','resnext50_32x4d']

# networks = ['resnet18','alexnet','vgg16','squeezenet1_0','densenet161','inception_v3',
# 'googlenet','shufflenet_v2_x1_0','mobilenet_v2','resnext50_32x4d','wide_resnet50_2',
# 'mnasnet1_0']

for name in networks:
    # for seed in [1234, 1235, 1236]:
    for seed in [1234]:
        # for pretrained in ['yes', 'no']:
        # for pretrained in ['yes']:
        for pretrained in ['no']:
            for thetaIdx in [(0,-1),(-10000,-1)]:
            # for thetaIdx in [(0,-1),(0,10000),(-10000,-1)]:
            # for thetaIdx in [(0,-1)]:
                params = [name, numRows, numCols, numJ, seed, 
                          pretrained, thetaIdx[0], thetaIdx[1]]
                command = 'sbatch submit.sh runner.ipynb '
                command += 'runner_%s_%d_%d_%d_%d_%s_%d_%d '%tuple(params)
                yaml = """ "{name: %s,numRows: %d,numCols: %d,numJ: %d,seed: %d,pretrained: %s,thetaStartIndex: %d,thetaEndIndex: %d,database: %s}" """%tuple(params+[database])
                command += '-y %s '%(yaml,)
                print(command)
                
                subprocess.call(command, shell=True)
