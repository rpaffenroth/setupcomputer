# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.2'
#       jupytext_version: 1.2.4
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# %%
#database = 'sqlite:////home/rcpaffenroth/data/friday_9-13-19/mydatabase_many_friday.db'
database = 'sqlite:////home/rcpaffenroth/data/good_9-20-19/mydatabase_many_thursday.db'


# %%
import dataset
import pickle
import pandas as pa
import numpy as np
import matplotlib.pylab as py
import collections

# %%
db = dataset.connect(database)
table = db['runs']
result = table.all()
dfOrig = pa.DataFrame(result)

# %%
df = dfOrig[dfOrig['pretrained']=='yes']

# %%
colors = ['r','g','b','c','m','y','k','#555555','#dddddd']
colorMap = dict(zip(df['name'].unique(),colors))

# %%
colorMap

# %%
dimensions=collections.defaultdict(list)
normalize=True
for index, row in df.iterrows():
    Js = pickle.loads(row['Js'])
    for J in Js:
        if normalize:
            for i in range(J.shape[0]):
                norm = np.linalg.norm(J[i,:])
                if norm > 0:
                    J[i,:] /= norm 
        U,E,VT = np.linalg.svd(J)
        # if row['name'] in ['alexnet','densenet161','resnext50_32x4d']:
        # if row['name'] in ['densenet161'] and row['seed'] in [1234,1235]:
        if True:
            py.plot((E/E[0]), colorMap[row['name']])
        for i in range(len(E)):
            if E[0] > 0.0 and E[i]/E[0] < 0.1:
                dimensions[row['name']] += [i]
                break

# %%
errors = pa.read_csv('errors.csv')
dimVsError = []
for key in dimensions.keys():
    dim = np.mean(dimensions[key])
    error = float(errors[errors['name']==key]['best5'])
    print(key, dim, error)
    dimVsError +=[[dim,error]]
dimVsError = np.array(dimVsError)
py.scatter(dimVsError[:,0], dimVsError[:,1])

# %%
EAverage=collections.defaultdict(lambda : np.zeros(df['numCols'].iloc[0]))
for index, row in df.iterrows():
    Js = pickle.loads(row['Js'])
    for J in Js:
        if normalize:
            for i in range(J.shape[0]):
                norm = np.linalg.norm(J[i,:])
                if norm > 0:
                    J[i,:] /= norm 
        U,E,VT = np.linalg.svd(J)
        EAverage[row['name']] += E/E[0]

_,ax = py.subplots(ncols=2,figsize=[16,8])
for key in EAverage.keys():
    ax[0].plot(EAverage[key],colorMap[key])
    ax[1].semilogy(EAverage[key],colorMap[key])
    
ax[0].legend(EAverage.keys())
ax[1].legend(EAverage.keys())


# %%
errors = pa.read_csv('errors.csv')
dimVsError = []
for key in EAverage.keys():
    dim = np.sum(EAverage[key]/EAverage[key][0])
    error = float(errors[errors['name']==key]['best1'])
    print(key, dim, error)
    dimVsError +=[[dim,error]]
dimVsError = np.array(dimVsError)
py.scatter(dimVsError[:,0], dimVsError[:,1])

# %%

# %%
