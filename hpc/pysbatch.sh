#!/bin/bash
# One node
#SBATCH -N 1
# One job on that node
#SBATCH -n 1
# Please give me a GPU
#SBATCH --gres=gpu
# We keep the log separately
#SBATCH -o /dev/null

filename="$1"
shift 1
label="$1"
shift 1
now=$(date +"%F_%T")
dir=$(mktemp -d $HOME/data/${label}_${now}.XXXXX)
cp $filename $dir
cd $dir
mkdir -p models
echo "$dir $(basename $filename) $@" > log
$HOME/miniconda3/envs/deeplearning/bin/python $(basename $filename) "$@" >> log 2>&1 


