#!/bin/bash
#SBATCH --job-name=jupyternotebook_rcpaffenroth
#SBATCH --output=/home/rcpaffenroth/tmp/jupyter.log
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --mem=8gb
#SBATCH --gres=gpu:1

# The jupyter notebook needs this to have write permissions
# to its temp directory
export XDG_RUNTIME_DIR=""

port=$(shuf -i 6000-7000 -n 1)

echo -e "\nStarting Jupyter on port ${port} on the $(hostname) server."
echo -e "\nSSH tunnel command: ssh -NL 6600:$(hostname):${port} ${USER}@ace.wpi.edu"
echo -e "\nLocal URI: http://localhost:6600"

echo $port > /home/rcpaffenroth/tmp/jupyter.port
echo $(hostname) > /home/rcpaffenroth/tmp/jupyter.hostname

# Setup a port forward to rcp.wpi.edu so that I can access this from outside
ssh -f -N -R 6600:localhost:${port} rcp.wpi.edu
# This is likely illegal!   I mean, I could take over SSH this way.
# ssh -f -N -R 2200:localhost:22 rcp.wpi.edu

# Obviously change this line to match your user notebook-dir
$HOME/miniconda3/envs/deeplearning/bin/jupyter lab --notebook-dir=/home/rcpaffenroth/projects --no-browser --port=${port} --ip=0.0.0.0 
