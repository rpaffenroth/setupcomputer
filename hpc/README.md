# Notes

To set a password:
jupyter notebook password

To start the jupyter process:
ace_sshSbatchJupyer.sh

To cleanup the jupyter process:
ace_sshCleanJupyer.sh

Setup the local ssh connection:
setupSSHConnection.py

To run a python program on turing.wpi.edu:
sbatch turing_sbatch.sh labelname foo.py

# Directories
benchmark - mainly looking to see if GPU is available
runner - a nice template for running code on Turing, mainly focused on jupyter notebooks
