#!/usr/bin/env python
import subprocess
import json

subprocess.call('rsync -r ace.wpi.edu:projects/setupcomputer/hpc/log .', shell=True)

data = json.load(open('log/jupyter.json','r'))

command = 'ssh -NL 6600:%s:%s ace.wpi.edu'%(data['hostname'], data['port'])

subprocess.call(command, shell=True)



