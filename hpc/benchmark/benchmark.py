from __future__ import print_function
from timeit import default_timer as timer
import collections
timers = collections.defaultdict(float)

# Run this to generate profiler information for CPU  and GPU
# python -m cProfile -s cumulative benchmark.py --no-cuda > no_cuda.prof && python -m cProfile -s cumulative benchmark.py > cuda.prof

# Look at the profiler timings in a compact way
# echo "no cuda" && grep benchmark.py no_cuda.prof && echo "cuda" && grep benchmark.py cuda.prof

# Get just my timing information
# echo "no cuda" && grep defaultdict no_cuda.prof && echo "cuda" && grep defaultdict cuda.prof
 
start = timer()
import argparse
import torch
import torch.utils.data
from torch import nn, optim
from torch.nn import functional as F
from torchvision import datasets, transforms
from torchvision.utils import save_image
timers['imports'] += timer()-start


start = timer()
parser = argparse.ArgumentParser(description='VAE MNIST Example')
parser.add_argument('--batch-size', type=int, default=128, metavar='N',
                    help='input batch size for training (default: 128)')
parser.add_argument('--epochs', type=int, default=10, metavar='N',
                    help='number of epochs to train (default: 1)')
parser.add_argument('--batches', type=int, default=5, metavar='N',
                    help='number of batches per epoch (default: 5)')
parser.add_argument('--no-cuda', action='store_true', default=False,
                    help='enables CUDA training')
parser.add_argument('--seed', type=int, default=1, metavar='S',
                    help='random seed (default: 1)')
parser.add_argument('--log-interval', type=int, default=10, metavar='N',
                    help='how many batches to wait before logging training status')
args = parser.parse_args()
args.cuda = not args.no_cuda and torch.cuda.is_available()

torch.manual_seed(args.seed)

device = torch.device("cuda" if args.cuda else "cpu")

if args.cuda:
    print('CUDA available')
    print('current device:',torch.cuda.current_device())
    print('device count:',torch.cuda.device_count())
    print('device name:',torch.cuda.get_device_name(0))
else:
    print('CUDA *not* available')
timers['args'] += timer()-start

start = timer()
kwargs = {'num_workers': 1, 'pin_memory': True} if args.cuda else {}

# train_loader = torch.utils.data.DataLoader(
#     datasets.MNIST('tmp_data', train=True, download=True, transform=transforms.ToTensor()),
#     batch_size=args.batch_size, shuffle=True, **kwargs)

# test_loader = torch.utils.data.DataLoader(
#     datasets.MNIST('tmp_data', train=False, transform=transforms.ToTensor()),
#     batch_size=args.batch_size, shuffle=True, **kwargs)

train_loader = torch.utils.data.DataLoader(
    datasets.FakeData(size=256, image_size=(1, 28, 28), transform=transforms.ToTensor()),
    batch_size=args.batch_size, shuffle=True, **kwargs)

test_loader = torch.utils.data.DataLoader(
    datasets.FakeData(size=256, image_size=(1, 28, 28), transform=transforms.ToTensor()),
    batch_size=args.batch_size, shuffle=True, **kwargs)

timers['data loader'] += timer()-start


start = timer()
class AE(nn.Module):
    def __init__(self, sizes = [784, 10000, 10000, 10000, 400, 20, 400, 784]):
        super(AE, self).__init__()

        layers = []
        for i in range(len(sizes)-1):
            layers += [nn.Linear(sizes[i], sizes[i+1])]
        self.layers = nn.ModuleList(layers)

    def forward(self, x):
        x = x.view(-1, 784)
        for layer in self.layers:
            x = layer(x)
            x = F.relu(x)
        return torch.sigmoid(x)

model = AE().to(device)
timers['module and to device'] += timer()-start

start = timer()
optimizer = optim.Adam(model.parameters(), lr=1e-3)
timers['create optimizer'] += timer()-start

# Reconstruction + KL divergence losses summed over all elements and batch
def loss_function(recon_x, x):
    BCE = F.binary_cross_entropy(recon_x, x.view(-1, 784), reduction='sum')
    return BCE


def train(epoch, batches=None):
    start = timer()
    model.train()
    train_loss = 0
    timers['train:setup'] += timer()-start

    for batch_idx, (data, _) in enumerate(train_loader):
        start = timer()
        data = data.to(device)
        timers['train:data.to'] += timer()-start

        start = timer()
        optimizer.zero_grad()
        recon_batch = model(data)
        loss = loss_function(recon_batch, data)
        timers['train:opt setup'] += timer()-start

        start = timer()
        loss.backward()
        train_loss += loss.item()
        optimizer.step()
        timers['train:back and opt step'] += timer()-start

        start = timer()
        if batch_idx % args.log_interval == 0:
            print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                epoch, batch_idx * len(data), len(train_loader.dataset),
                100. * batch_idx / len(train_loader),
                loss.item() / len(data)))
        if (not batches is None) and batch_idx > batches:
            break
        timers['train:printing'] += timer()-start

    start = timer()
    print('====> Epoch: {} Average loss: {:.4f}'.format(
          epoch, train_loss / len(train_loader.dataset)))
    timers['train:end'] += timer()-start


def test():
    start = timer()
    model.eval()
    test_loss = 0
    timers['test:setup'] += timer()-start
    
    with torch.no_grad():
        for i, (data, _) in enumerate(test_loader):
    
            start = timer()
            data = data.to(device)
            timers['test:data.to'] += timer()-start

            start = timer()
            recon_batch = model(data)
            timers['test:model'] += timer()-start

            start = timer()
            test_loss += loss_function(recon_batch, data).item()
            timers['test:loss'] += timer()-start

    start = timer()
    test_loss /= len(test_loader.dataset)
    print('====> Test set loss: {:.4f}'.format(test_loss))
    timers['test:end'] += timer()-start

for epoch in range(1, args.epochs + 1):
    start = timer()
    train(epoch, args.batches)
    timers['train'] += timer()-start

    start = timer()
    test()
    timers['test'] += timer()-start

print(timers)