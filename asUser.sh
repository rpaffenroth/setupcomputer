#! /bin/bash

sudo apt install git
if [ "$1" = "windows" ]; then
    GIT=git.exe
    $GIT config --global core.autocrlf true
else
    GIT=git
    $GIT config --global core.autocrlf input
fi

# Git setup
$GIT config --global user.email "randy.paffenroth@gmail.com"
$GIT config --global user.name "Randy Paffenroth"
$GIT config --global push.default simple
$GIT config --global merge.tool meld

# Needed for vi
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim

code --install-extension Shan.code-settings-sync

# Download some projects
cd $HOME/projects
git clone git@bitbucket.org:rcpaffenroth/setupcomputer
git clone git@bitbucket.org:rcpaffenroth/configurationfiles
git clone git@bitbucket.org:rcpaffenroth/deepbifurcation
git clone git@bitbucket.org:rcpaffenroth/deepadvcca

# The user needs to run this, since passwords are generated and SSH keys copied
#cp $HOME/setupcomputer/RUNME.sh $HOME/RUNME.sh







