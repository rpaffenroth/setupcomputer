# List the available images
LxRunOffline.exe l

# Name of image to use
$image="C:\Users\randy\OneDrive\Applications\Ubuntu-20.04.tar.gz"
$name="Ubuntu-20.04"

# Install a particular image
LxRunOffline.exe i -n $name -d C:\Local\WSL\$name -f $image

# Make me the default user
LxRunOffline.exe su -n $name -v 1000

# Make a shortcut on the desktop
LxRunOffline.exe s -n $name -f C:\Users\randy\OneDrive\Desktop\$name.lnk -i C:\Users\randy\OneDrive\Applications\icons\Martz90-Circle-Ubuntu.ico

# Change to version 2
wsl --set-version $name 2