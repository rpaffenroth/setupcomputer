# Install ssh

# May not need this after 5/28 update, this is to fix a 
# bug with jump hosts.   See README.md for path.
# choco.exe install -y openssh

# Install immediate packages
choco.exe install -y firefox 
choco.exe install -y microsoft-edge
choco.exe install -y lxrunoffline 
choco.exe install -y vscode
choco.exe install -y git.install
choco.exe install -y terminus
choco.exe install -y powertoys
choco.exe install -y microsoft-windows-terminal
choco.exe install -y authy-desktop

# Fonts
choco.exe install -y hackfont
choco.exe install -y inconsolata
choco.exe install -y firacode


# Install immediate packages
choco.exe upgrade -y firefox 
choco.exe upgrade -y microsoft-edge
choco.exe upgrade -y lxrunoffline 
choco.exe upgrade -y vscode
choco.exe upgrade -y git.install
choco.exe upgrade -y terminus
choco.exe upgrade -y powertoys
choco.exe upgrade -y microsoft-windows-terminal
choco.exe install -y authy-desktop

# Fonts
choco.exe upgrade -y hackfont
choco.exe upgrade -y inconsolata
choco.exe upgrade -y firacode







