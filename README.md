# Login
rcpaffenroth@wpi.edu  
...4E.....e

# CUDA debugging

```
[~/singularity_images ]
compute-0-25$ python pytorchUsingGPU.py
CUDA *not* available
[~/singularity_images ]
compute-0-25$ module load cuda10.1/toolkit
[~/singularity_images ]
compute-0-25$ python pytorchUsingGPU.py
CUDA available
0
<torch.cuda.device object at 0x2aab05ac5d90>
4
Tesla K80
[~/singularity_images ]
compute-0-25$ nvidia-smi
Fri Dec  4 05:29:40 2020
+-----------------------------------------------------------------------------+
| NVIDIA-SMI 450.80.02    Driver Version: 450.80.02    CUDA Version: 11.0     |
|-------------------------------+----------------------+----------------------+
| GPU  Name        Persistence-M| Bus-Id        Disp.A | Volatile Uncorr. ECC |
| Fan  Temp  Perf  Pwr:Usage/Cap|         Memory-Usage | GPU-Util  Compute M. |
|                               |                      |               MIG M. |
|===============================+======================+======================|
|   0  Tesla K80           On   | 00000000:04:00.0 Off |                    0 |
| N/A   26C    P8    27W / 149W |      0MiB / 11441MiB |      0%      Default |
|                               |                      |                  N/A |
+-------------------------------+----------------------+----------------------+
|   1  Tesla K80           On   | 00000000:05:00.0 Off |                    0 |
| N/A   29C    P8    30W / 149W |      0MiB / 11441MiB |      0%      Default |
|                               |                      |                  N/A |
+-------------------------------+----------------------+----------------------+
|   2  Tesla K80           On   | 00000000:84:00.0 Off |                    0 |
| N/A   25C    P8    27W / 149W |      0MiB / 11441MiB |      0%      Default |
|                               |                      |                  N/A |
+-------------------------------+----------------------+----------------------+
|   3  Tesla K80           On   | 00000000:85:00.0 Off |                    0 |
| N/A   30C    P8    29W / 149W |      0MiB / 11441MiB |      0%      Default |
|                               |                      |                  N/A |
+-------------------------------+----------------------+----------------------+

+-----------------------------------------------------------------------------+
| Processes:                                                                  |
|  GPU   GI   CI        PID   Type   Process name                  GPU Memory |
|        ID   ID                                                   Usage      |
|=============================================================================|
|  No running processes found                                                 |
+-----------------------------------------------------------------------------+
[~/singularity_images ]
compute-0-25$
```


# Mount research.wpi.edu on melnibone
This works as root

- mount -t cifs -o user=rcpaffenroth -o dom=ADMIN //research.wpi.edu/MA/rcpaffenroth /tmp/mnt+

This works as rcpaffenroth

- gio mount smb://research.wpi.edu/MA/rcpaffenroth
- ls /run/user/17114/gvfs/smb-share:server=research.wpi.edu,share=ma/rcpaffenroth/rpaffenroth

# Globus command

globus ls 24de70b4-1bff-11e8-b6f9-0ac6873fc732:/mnt/research/MA

# Commands to get docker exposed from turing node

Note

- 2222 comes from the ports that are open on the moc-gateway firewall
- 8022 comes from the defaul in the Dockerfile start-ssh.sh script

- docker$ ssh -N -R 2222:localhost:8022 moc-gateway
- onyxia$ ssh -p 2222 rcpaffenroth@moc-gateway

# Get a node from Turing
srun -p long --gres=gpu --constraint="K80|V100|P100|T4" --pty bash

# Edge setup for fullscreen and keyboard
Edge option to let the application has first crack at keys.

edge://flags/#system-keyboard-lock

# git credential caching
git config --global credential.helper 'cache --timeout=3600'

Set the cache to timeout after 1 hour (setting is in seconds)

This is very nice for https://bitbucket.org/rcpaffenroth/...   which is good for remove development

# ssh in container

rcpaffenroth@1005d7918561:/$ mkdir -p $HOME/tmp/etc/ssh

rcpaffenroth@1005d7918561:/$ ssh-keygen -A -f $HOME/tmp

ssh-keygen: generating new host keys: RSA DSA ECDSA ED25519

rcpaffenroth@1005d7918561:/$ /usr/sbin/sshd -D -h $HOME/tmp/etc/ssh/ssh_host_ed25519_key

# CUDA on stormbringer

This are the docs for installing the drivers

https://github.com/nvidia/nvidia-docker

https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html#docker

Note, the environment variables were what I was missing.  From https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/user-guide.html

docker run -e NVIDIA_VISIBLE_DEVICES=all  -e NVIDIA_DRIVER_CAPABILITIES=compute,utility  --gpus all -it rpaff/jupyterlab-ssh-9.2 bash

Note, it seems the environment variables are already in singularity.

# Getting started
- wget https://bitbucket.org/rcpaffenroth/setupcomputer/raw/HEAD/RUNME.sh 
- wget tinyurl.com/rcprunme
- wget bit.ly/rcprunme

# Links

"C:\Program Files\Microsoft VS Code\Code.exe" --folder-uri "vscode-remote://ssh-remote+moc-server/home/rcpaffenroth/projects"

"C:\Program Files\Microsoft VS Code\Code.exe" --folder-uri "vscode-remote://ssh-remote+compute/home/rcpaffenroth/projects"

C:\Users\randy\AppData\Local\Microsoft\WindowsApps\wt.exe -p Ubuntu-20.04

"C:\Program Files (x86)\Microsoft\Edge\Application\msedge.exe" --new-window http://localhost:6610

"C:\Program Files (x86)\Microsoft\Edge\Application\msedge.exe" --new-window http://localhost:6611

C:\Users\randy\AppData\Local\Microsoft\WindowsApps\wt.exe -p PowerShell

# Singularity on Turing

## Run on Turing

module load singularity/3.5.3/wekbgwg  
module load squashfs

* Note: may need to be run twice on Turing because of CPU limit  
* Note: can't be run on worker because limited /tmp size

singularity pull cuda-pytorch.sif docker://pytorch/pytorch:1.2-cuda10.0-cudnn7-devel  

or more basic image

singularity pull nvidia-base.sif docker://nvidia/cuda:10.1-base-ubuntu18.04

## Run on worker node

module load singularity/3.5.3/wekbgwg7

singularity run --nv cuda-pytorch.sif

Or you can do

singularity run --nv nvidia-base.sif

and run miniconda3!

# WSL 2 set DISPLAY
export DISPLAY=$(awk '/nameserver / {print $2; exit}' /etc/resolv.conf 2>/dev/null):0
export LIBGL_ALWAYS_INDIRECT=1

# Driver for Bluetooth 5.0 dongle
www.mytechkey.com/pages/driver

# Drive for brostrend wifi adapater for linux
https://deb.trendtechcn.com/

# Model number of Yoga 730
730-15IKB
## Screw type
M2.0 x 7.0 mm, Torx-head


# AWS
NVIDIA T4 

Instance Size 	vCPUs 	Memory (GB) 	GPU 	Storage (GB) 	Network Bandwidth (Gbps) 	EBS Bandwidth (GBps) 	On-Demand Price/hr* 	1-yr Reserved Instance Effective Hourly* (Linux) 	3-yr Reserved Instance Effective Hourly* (Linux)

g4dn.xlarge 	4 	16 	1 	125 	Up to 25 	Up to 3.5 	$0.526 	$0.316 	$0.210

# Turing/ace port forwarding
On the compute node 2224/22 is for ssh

compute-0-04$ ssh -N -R 2222:localhost:22 moc-gateway

Note, you don't actually need to directly log onto moc-gateway
Also, note that the 6600 port forwarding is handled by the ssh to moc-gateway from 
the client

Here is what you do on a compute node to make it accessible from the outside

ssh -N -R 2224:localhost:22 moc-gateway

# Turing/Ace knowing what hardward is actually there
sinfo -Nel

get a node with a T4

srun -p long --gres=gpu --constraint=T4 --pty bash

# Get ssh keys
git clone rcpaffenroth@have.rcpaffenroth.org:repos/ssh

mv .ssh .ssh.bak

mv ssh .ssh

# Install WSL

LxRunOffline.exe i -n Ubuntu-20.04 -d C:\Local\WSL\Ubuntu-20.04 -f C:\Local\WSL\Ubuntu-20.04.tar

LxRunOffline.exe su -n Ubuntu-20.04 -v 1000

LxRunOffline.exe s -n Ubuntu-20.04 -f c:\Users\randy\OneDrive\Desktop\Ubuntu-20.04.lnk

# Checklist
Start chromium and log in to sync account

onenote login

lastpass login

# Yoga pro 2

## For for the Yoga touchpad
https://github.com/awahlig/two-finger-scroll

## Drivers

Power

https://download.lenovo.com/consumer/mobiles/wwe3038e.exe

Touchpad

https://download.lenovo.com/consumer/mobiles/13kt01af.exe

# Directories

hpc - stuff for running on Turing

vnc - stuff for running X on remote machines

# setupcomputer

## Vagrant
https://www.serverlab.ca/tutorials/virtualization/how-to-auto-upgrade-virtualbox-guest-additions-with-vagrant/

## VS Code as git editor
git config --global core.editor "code --wait"

## Windows terminal with specific profile
wt -p "PowerShell Core"

## Order of scripts
Here is the order to run the scripts

beforeAdditions.sh:  The packages you need before installing the VirtualBox guest additions.

Install the guest additions here
--I snap shotted the machine at this point

See asRoot.sh for a description of the various options
for by hand and automated (e.g., vagrant) installs

asUser.sh:  Install the user end stuff.
RUNME.sh:  The final settings of passwords, downloading of keys, etc.

## Straight Virtualbox
When this was tested I installed the minimal Ubuntu ISO with
1) The cloud image packages
2) The lubuntu minimal install

## Running on turing.wpi.edu
You need to do

sbatch turing_sbatch.sh randyisdumb randyisdumb.py

## Python
Run the setup script as "source python.sh"  The script does
lots of environment variable stuff, and wants to run in the
login namespace (I think)

## Setting up windows checklist

**NEEDS TO BE ORDERED**

1. Onedrive - gives me cmder and portable apps
    - Be sure to do "Settings - Files-on-demand - Save space and download files as you use them"
1. Microsoft edge (the new one) is all setup to know who I am and install my extensions
    - Install using chocolatey (part of my packages already)
    - Setup lastpass
1. Setup authy desktop
    - Needs another device to validate
    - Needs backup password in lastpass
1. Microsoft Teams and Zoom
    - Zoom just self installs the first time you use it.
    - Microsoft Teams you can get by signing into online version and downloading there.
1. Get setupcomputer from bitbucket.org
1. Using cmdr you can
    - cp p:/ssh/* c:/Users/randy/.ssh
1. Install chocolatey from chocolatey.org
    - windows_1_run_in_powerhshell.ps1   (also does WSL)
    - windows_2_small_packages.bat to get basic chocolatey packages
    - windows_3_packages.bat to get bigger chocolatey packages
    - Gives Steam for games
    - Gives VirtualBox
        - Use "black" USB to get VM
        - Setup bios for VMs
    - Setup authy
1. Setup email
    - For example, the windows email app
1. Setup todoist and desktop app 
1. Install Microsoft office
1. Install the Windows Linux Subsystem
    - Pengwin and x410 from microsoft store
    - wslconfig.exe /upgrade WLinux    <=== this upgrades the file system
    - Can use asRoot.sh and asUSer.sh to get setup
1. Install PDF Annotator
1. SSH agent being part of windows.  See https://code.visualstudio.com/docs/remote/troubleshooting
```
    # Make sure you're running as an Administrator
    Set-Service ssh-agent -StartupType Automatic
    Start-Service ssh-agent
    Get-Service ssh-agent
```
1.  For keyboard and mouse need to setup Razer.
    - Login - 

## Old notes
1. SSH the old way (not needed now since windows 10 has a pretty complete implementation with ssh, scp, ssh-agent)
    - If all you have is one private key, simply append its full path to your "Target" field like so:
    Target: "C:\Program Files\PuTTY\pageant.exe" C:\Path\to\myKeys\MyKey.ppk
    - Set GIT_SSH to plink.exe in the putty installation
    You can just search for "environment variables" in Cortana to get
    to the right place
    - Possible error with "Store key in cache? (y/n) y" hang see link below
    - https://stackoverflow.com/questions/33240137/git-clone-pull-continually-freezing-at-store-key-in-cache

1. Python (not done these days, since WSL is better and I have mournblade)
    - Install CUDA and cudann
        - 9.0 for CUDA is a package
        - cuDNN is files you need to copy around
        - The default locations seem to work on and don't need any variables set

1. I am torn between 

    1) apt-get Python packages
    2) pip
    3) Anaconda

I think that apt-get Python packages for the basic stuff, and pip for the rest seems to be what tensorflow prefered at one time.
However, Jupyter prefers Anaconda.  Also, I have python.sh in setup computer for just this purpose now.
