#! /bin/bash

ansible-galaxy install -r requirements.yml
ansible-playbook -i localhost_inventory.ini full.yml --tags "personal"
