# NOTES

# To get the external roles.
ansible-galaxy install -r requirements.yml

# Run locally
ansible-playbook --ask-become-pass local.yml

# Current test
ansible-playbook -i inventory.ini -l moc-gateway --ask-become-pass full_v2.yml

