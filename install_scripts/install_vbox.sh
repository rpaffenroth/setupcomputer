#! /bin/bash

# From https://phoenixnap.com/kb/install-virtualbox-on-ubuntu
sudo apt-get install software–properties–common
wget –q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | sudo apt-key add -
# wget -q https://www.virtualbox.org/download/oracle_vbox.asc -O- | sudo apt-key add –
sudo add-apt-repository "deb http://download.virtualbox.org/virtualbox/debian bionic contrib"
sudo apt-get update
sudo apt-get install virtualbox–6.1

wget https://download.virtualbox.org/virtualbox/6.1.4/Oracle_VM_VirtualBox_Extension_Pack-6.1.4.vbox-extpack

sudo VBoxManage extpack install --accept-license=56be48f923303c8cababb0bb4c478284b688ed23f16d775d729b89a2e8e5f9eb Oracle_VM_VirtualBox_Extension_Pack-6.1.4.vbox-extpack


