#! /bin/sh

wget https://releases.hashicorp.com/vagrant/2.2.7/vagrant_2.2.7_linux_amd64.zip
unzip vagrant_2.2.7_linux_amd64.zip
./vagrant plugin install vagrant-vbguest
./vagrant plugin install vagrant-disksize