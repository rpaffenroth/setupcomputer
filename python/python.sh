#! /bin/bash
CONDAURL=https://repo.anaconda.com/archive/Anaconda3-2019.03-Linux-x86_64.sh
CONDANAME=Anaconda3-2019.03-Linux-x86_64.sh
CONDAROOT=$HOME/anaconda3

echo "remember that you need to 'source python.sh'\n\n\n"

CONDAURL=https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
CONDANAME=Miniconda3-latest-Linux-x86_64.sh
if [ "$1" == "turing" ] ; then
    # For turing and ace
    CONDAROOT=/work/rcpaffenroth/miniconda3
else
    CONDAROOT=$HOME/miniconda3
fi

CONDABIN=$CONDAROOT/bin

# Install anaconda if it is not already installed
if [ ! -d $CONDAROOT ] ; then
    wget $CONDAURL
    mkdir -p $HOME/Downloads
    mv $CONDANAME $HOME/Downloads
    bash $HOME/Downloads/$CONDANAME -b -p $CONDAROOT
fi

# Since we are doing a batch install we need to 
$HOME/miniconda3/bin/conda init
source ~/.bashrc
conda activate base

# Creat the environment I work in
# conda create -y -n deeplearning python=3.7
conda create -y -n deeplearning python=3.6
conda activate deeplearning

echo "installs both GPU and CPU  version"
# There is a sorry state of affairs with ace/turing and pytorch.  Specifically, k20s are being deprecated
# and they are the line share of the cards on ace/turing.   I had nice conversation with James about the
# issue and there is a refresh in progress.  However, until then I ande my students need to be careful about
# upgrading.   The 1.1 and 1.2 lines below work as of 2/13/2020.   Note the explicit verion of pillow in needed since
# when pillow updated to 7.0 they introduced a change that breaks old versions of pytorch.
# # Working line for pytorch 1.1
# conda install pytorch==1.1.0 torchvision==0.3.0 pillow==6.2 cudatoolkit=9.0 -c pytorch
# Working line for pytorch 1.2
# This is the last version that works with a K20
conda install -y -c pytorch pytorch==1.2.0 torchvision==0.4.0 pillow==6.2 cudatoolkit=9.2
# Old line for most recent version of pytorch
# conda install -y -c pytorch pytorch torchvision cudatoolkit=9.2 

# Install Jupyter lab
# conda install -y jupyterlab=1
# conda install -y jupyterlab=2.0

# conda install -y nodejs jupyter rise jupyter_contrib_nbextensions jupyter_nbextensions_configurator jupyterthemes plotly holoviews scikit-learn sympy networkx bokeh pandas jedi flake8 autopep8 yapf pytest jupyter_dashboards rise jupytext ipywidgets ipympl widgetsnbextension jupyterthemes ipython nbval papermill numba
conda install -y -c conda-forge nodejs=12 jupyter rise holoviews scipy scikit-learn sympy networkx bokeh pandas jedi flake8 autopep8 yapf pytest rise nbval papermill numba pyscaffold statsmodels seaborn boto3 psutil qgrid pyarrow pymongo ipywidgets ipympl nbval papermill pyscaffold jupyterlab=2.1 pytables h5py jupytext ipykernel conda-pack

conda install -y -c coecms dataset 

# Stuff for scaffolding out projects
# pip install pyscaffoldext-dsproject pyscaffoldext-markdown pre-commit

# Enable jupytext for the server
jupyter serverextension enable jupytext

# Install jupyterlab and its extensions
jupyter labextension install --minimize=False @jupyter-widgets/jupyterlab-manager
jupyter labextension install --minimize=False @jupyterlab/toc
jupyter labextension install --minimize=False jupyter-matplotlib
jupyter labextension install --minimize=False @bokeh/jupyter_bokeh
jupyter lab build 

# jupyter labextension install qgrid2









