## How to use

Vagrantfile:   This is used bby virtualbox to define the machine.  It also bootstraps the ansible provisioning.
login_rcpaffenroth.sh:  Just a small script to log into the VM from windows
for_wsl.sh:  NOT SUPPORTED, but get WSL setup to use ansible and vagrant in WSL, but virtualbox in Windows 10.
install_scripts:  A not working attempt to get a VM that can reproduce.  I.e., install everything in a VM and then use that VM to make a new VM
ansible_playbook:  The provisioning code.  This is were the action is. 