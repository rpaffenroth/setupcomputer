#! /bin/sh

sudo apt install ansible vagrant

export VAGRANT_WSL_ENABLE_WINDOWS_ACCESS="1"

vagrant plugin install vagrant-vbguest
vagrant plugin install vagrant-disksize

# export PATH="$PATH:/mnt/c/Program Files/Oracle/VirtualBox"