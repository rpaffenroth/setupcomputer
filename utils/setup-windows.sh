#! /bin/bash

EDGE="/mnt/c/Program Files (x86)/Microsoft/Edge/Application/msedge.exe"
# VSCODE="/mnt/c/Program Files/Microsoft VS Code/Code.exe"

if [ "$1" == "moc-server" ]; then
    "$EDGE" --new-window http://localhost:6610
    code --folder-uri "vscode-remote://ssh-remote+moc-server/home/rcpaffenroth/projects" 
    ssh -N moc-server
fi

if [ "$1" == "compute" ]; then
    "$EDGE" --new-window http://localhost:6611
    code --folder-uri "vscode-remote://ssh-remote+compute/home/rcpaffenroth/projects" 
    ssh -N compute
fi


