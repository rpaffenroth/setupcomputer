#! /bin/bash

# This can be gotten using
#
#  wget https://bitbucket.org/rcpaffenroth/setupcomputer/raw/HEAD/RUNME.sh 
#

umask 022

# I need git and ansible to bootstrap the installation.
sudo apt update
sudo apt install git ansible python

# Get my ssh keys
export GIT_SSH_COMMAND="ssh -o StrictHostKeyChecking=no"
rm -rf $HOME/.ssh.bak $HOME/ssh 
git clone rcpaffenroth@haven.rcpaffenroth.org:repos/ssh.git $HOME/ssh
mv $HOME/.ssh $HOME/.ssh.bak
mv $HOME/ssh $HOME/.ssh
sh $HOME/.ssh/permissions.sh
eval `ssh-agent`
ssh-add $HOME/.ssh/id_rsa

# Get setupcompter, since that is where the ansible playbooks are
export GIT_SSH_COMMAND="ssh -o StrictHostKeyChecking=no"
mkdir -p $HOME/projects
git clone git@bitbucket.org:rcpaffenroth/setupcomputer $HOME/projects/setupcomputer

# Let ansible handle the rest
bash ./local_ansible.sh




